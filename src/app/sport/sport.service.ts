import { Injectable, computed, inject, signal } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Sport } from './sport';
import { catchError, of } from 'rxjs';
import { toSignal } from '@angular/core/rxjs-interop';

@Injectable({
  providedIn: 'root',
})

export class SportService {
  public sport: Sport[] = [];

  http = inject(HttpClient);
  ball: string = '';

  /**
   * 取得表格內容
   */
  private path = '/assets/mock/sport.json';
  private getdata$ = this.http.get<Sport[]>(this.path).pipe(
    catchError(() => of([] as Sport[])) //  on any error, just return an empty array
  );

  //將狀態公開為信號
  sports = toSignal(this.getdata$, {initialValue: [] as Sport[]});


}


