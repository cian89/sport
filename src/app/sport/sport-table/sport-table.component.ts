import { SportService } from './../sport.service';
import { Component, inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TableModule } from 'primeng/table';
import { SportComponent } from '../sport.component';

@Component({
  selector: 'app-sport-table',
  standalone: true,
  imports: [
    CommonModule,
    TableModule,
  ],
  providers: [SportService],
  templateUrl: './sport-table.component.html',
  styleUrls: ['./sport-table.component.scss']
})
export class SportTableComponent {
  cols = [
    { field: '球種',header: '球種'},
    { field: '日期',header: '日期'},
    { field: '時間',header: '時間'},
    { field: '賽事',header: '賽事'},
    { field: '隊伍(主場/客場)',header: '隊伍(主場/客場)'},
    { field: '比分',header: '比分'},
];

  dataService = inject(SportService);
  sport = inject(SportComponent);

  ball = this.dataService.sports;

  data(){
    if(this.sport.filteredData != null){
      return this.sport.filteredData;
    }else{
      return this.ball();
    }
  }
}
