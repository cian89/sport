import { Component,computed,effect,inject, signal } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SportTableComponent } from './sport-table/sport-table.component';
import { SportService } from './sport.service';
import { HttpClientModule } from '@angular/common/http'

//primeng
import { ToolbarModule } from 'primeng/toolbar';
import { ButtonModule } from 'primeng/button';
import { InputTextModule } from 'primeng/inputtext';


@Component({
  selector: 'app-sport',
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    SportTableComponent,
    //primeng
    ToolbarModule,
    ButtonModule,
    InputTextModule,
    HttpClientModule

  ],
  providers: [SportService],
  templateUrl: './sport.component.html',
  styleUrls: ['./sport.component.scss']
})
export class SportComponent {
  public balltype: string = '';
  public sport: any;
  public errorMessage: any;
  buttontitle = [
    { label: '全部', id: 1 },
    { label: '棒球', id: 2 },
    { label: '足球', id: 3 },
    { label: '網球', id: 4 },
    { label: '籃球', id: 5 }
  ];
  filteredData:any;
  balls: any;

  dataService = inject(SportService);

  ball = this.dataService.sports;

  //篩選
  buttonAction(button: any) {
    let selectedlabel = button.label;
    this.filteredData = this.ball();
    if (button.id === 1) {
      this.dataService.sports = this.filteredData;
      console.log('Filtered data:', this.dataService.sports);
    } else {
      this.filteredData = this.filteredData.filter((item: any) => item.balltype === selectedlabel);
      this.dataService.sports = this.filteredData;
      console.log('Filtered data:', this.dataService.sports);
    }
  }

}
