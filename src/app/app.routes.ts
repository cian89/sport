import { Routes } from '@angular/router';

export const routes: Routes = [
  { path: '' , redirectTo: 'Sport' , pathMatch: 'full' },
  { path: 'Sport' , loadComponent: () => import('./sport/sport.component').then(m => m.SportComponent) }
]
